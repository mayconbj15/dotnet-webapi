#### Descrição do merge
- Descrever as mudanças que serão feitas nesse merge 

#### Checklist pra não esquecer nada pro deploy :sweat_smile:
##### Documentação
- [ ] **[UAT | PRD]** Documentação do projeto (confluence | swaager | diagramas)
- [ ] **[PRD]** Adicionar evidência de sucesso em UAT na GMS

##### Configuração
- [ ] **[UAT | PRD]** Roles do ambiente    ([projeto das roles](https://gitlab.sharedservices.local/plataformas/iac/crm))
- [ ] **[PRD]** **values.yaml** do  [microservice-configurations](https://gitlab.sharedservices.local/crm/microservice-configurations) correto e atualizado para o ambiente (UAT | PRD)

##### Outros
- [ ] Mencionar o card do JIRA 
- [ ] Fechar o Issue